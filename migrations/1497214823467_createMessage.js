exports.up = (pgm) => {
  pgm.createTable('messages', {
    id: 'id',
    to_id: {
      type: 'integer',
      references: 'users',
      notNull: true,
    },
    from_id: {
      type: 'integer',
      references: 'users',
      notNull: true,
    },
    created_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('NOW()'),
    },
    body: {
      type: 'varchar(5000)',
      notNull: true
    },
    seen: {
      type: 'boolean',
      notNull: true,
      default: 'false',
    }
  });
};

exports.down = (pgm) => {
  pgm.dropTable('messages');
};
