exports.up = (pgm) => {
  pgm.createTable('appearances', {
    id: 'id',
    user_id: {
      type: 'integer',
      references: 'users',
      notNull: true,
      onDelete: 'cascade',
      onUpdate: 'cascade'
    },
    lat: {
      type: 'real',
      notNull: true
    },
    lon: {
      type: 'real',
      notNull: true
    },
    created_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('NOW()'),
    }
  });
  pgm.createIndex('appearances', 'user_id');
  pgm.createIndex('appearances', ['lat', 'lon']);
  pgm.createIndex('appearances', ['user_id', 'created_at']);
};

exports.down = (pgm) => {
  pgm.dropTable('appearances');
};
