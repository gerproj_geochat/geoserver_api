exports.up = (pgm) => {
  pgm.createTable('posts', {
    id: 'id',
    nick: 'varchar(20)',
    facebook_id: 'varchar(512)',
    msg: {
      type: 'VARCHAR(255)',
      notNull: true,
    },
    creation_ts: {
      type: 'TIMESTAMP',
      notNull: true,
      default: pgm.func('NOW()'),
    },
    lat: {
      type: 'REAL',
      notNull: true,
    },
    lon:{
      type: 'REAL',
      notNull: true
    }
  });

  pgm.createIndex('posts', ['lat', 'lon', 'creation_ts']);
};

exports.down = (pgm) => {
  pgm.dropIndex('posts', ['lat', 'lon', 'creation_ts']);
  pgm.dropTable('posts');
};
