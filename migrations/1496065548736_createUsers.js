exports.up = (pgm) => {
  pgm.createTable('users', {
    id: 'id',
    profile: {
      type: 'jsonb',
      notNull: true
    },
    facebook_id: 'varchar(512)'
  });
  pgm.createIndex('users', 'facebook_id', {unique: true});
};

exports.down = (pgm) => {
  pgm.dropTable('users');
};
