exports.up = (pgm) => {
  pgm.alterColumn('posts', 'nick', {type: 'varchar(256)'});
};

exports.down = (pgm) => {
  pgm.alterColumn('posts', 'nick', {type: 'varchar(21)'});
};
