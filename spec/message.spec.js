var message = require("../src/message");
var _ = require("underscore");
var async = require("async");

var MSGS_SECTOR1 = 40,
		MSGS_SECTOR2 = 40;

var MAX_MSG_RETURNED = message.MAX_MSG_RETURNED;

var c = [{ lat: 11.2230, lon: 40.8823 }, {lat: 11.2233, lon: 40.8822}],
	//cFar can be in the same longitude, but the latitude sector will be different
	cFar = [{lat: 11.2248, lon: 40.8822}, {lat: 11.2249, lon: 40.8824}];

var sector1, sector2;
var nothing = function(){};

describe('Posting messages.', function() {
	it("Should populate an retrieve messages", function(done) {
		//Populates room 1
		expect(1).toEqual(2);
		async.series([
			function (callback) {
				sector1 = _.times(MSGS_SECTOR1, populate(c, function(data) {
					expect(data).toBe(true);
					callback();
				}));
			},

			function (callback) {

				sector2 = _.times(MSGS_SECTOR2, populate(cFar, function(data) {

					expect(data).toBe(true);
					callback();
				}));
			},

			function (callback) {
				setTimeout(function() {
					message.getInLocation(c[1], 0, function(err, data) {


						sector1.reverse().slice(0, MAX_MSG_RETURNED)
							.map(function(next) {
								return _.omit(next, 'location');
							})
							.forEach(function(next) {
								expect(data['posts'].slice(0, sector1.length).map(removeTimestamp)).toContain(next);
							});

						callback();
					});
				}, 1000);

			},

			function (callback) {

				setTimeout(function() {
					message.getInLocation(cFar[0], 0, function(err, data) {
								sector2.reverse().slice(0, MAX_MSG_RETURNED)
									.map(function(next) {
										return _.omit(next, 'location');
									})
									.forEach(function(next) {
										expect(data['posts'].slice(0, sector2.length).map(removeTimestamp)).toContain(next);
									});

								done();
								callback();
							})
				}, 1000);
			}

		]);

	});

	function removeTimestamp(post) {
		return _.omit(post, 'timestamp');
	}

});

describe("Nickname constrains", function() {
	it("Should have min lenght of 3", function() {
		expect(message.isNickValid("abc")).toBe(true);
		expect(message.isNickValid("ab")).toBe(false);
		expect(message.isNickValid("abcd")).toBe(true);

	});

	it("should be a string", function() {
		expect(message.isNickValid("")).toBe(false);
		expect(message.isNickValid(123)).toBe(false);
		expect(message.isNickValid([1, 2, 3, 5])).toBe(false);
		expect(message.isNickValid({ foo: 'bar' })).toBe(false);
	});

	it("Should have max lenght of 20", function() {
		expect(message.isNickValid( _.times(20, getChar).join('')) ).toBe(true);
		expect(message.isNickValid( _.times(21, getChar).join('')) ).toBe(false);
		expect(message.isNickValid( _.times(19, getChar).join('')) ).toBe(true);
	});
});

describe("Message constrains", function() {
	it("Should have min lenght of 3", function() {
		expect(message.isMessageValid("a")).toBe(true);
		expect(message.isMessageValid("")).toBe(false);
		expect(message.isMessageValid("abcd")).toBe(true);
	});

	it("Should have max lenght of 20", function() {
		expect(message.isMessageValid( _.times(255, getChar).join('')) ).toBe(true);
		expect(message.isMessageValid( _.times(256, getChar).join('')) ).toBe(false);
		expect(message.isMessageValid( _.times(254, getChar).join('')) ).toBe(true);
	});

	it("should be a string", function() {
		expect(message.isMessageValid("")).toBe(false);
		expect(message.isMessageValid(123)).toBe(false);
		expect(message.isMessageValid([1, 2, 3, 5])).toBe(false);
		expect(message.isMessageValid({ foo: 'bar' })).toBe(false);
		expect(message.isMessageValid(function(){})).toBe(false);
	});
});



function getChar() {
	return 'a';
}

function populate(sector, callback) {
	return function() {
		var msg = randomMsg();
		message.postInLocation( sector[Math.floor(Math.random() * 2)], msg, function(err, data) {
			callback(data);
		}  );
		return msg;
	}
}


function MakeMsg(nick, msg) {
	return {"nick": nick, "msg": msg};
}

function randomMsg() {
	return new MakeMsg(randomString(10), randomString(40));
}

function randomString(len)
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < len; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
