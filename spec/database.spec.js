var db = require('../src/database');

describe("PostgreSQL connection string", function() {
	it("Should be set to development database", function() {
		expect(db.pgConnectionString).toBe('postgres://localhost:5432/geo_chat');
	});
});
