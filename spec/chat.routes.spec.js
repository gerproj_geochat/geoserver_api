
var request = require('supertest')
  , express = require('../app'),
  _ = require('underscore');

var app = express.app;


describe('GET messages', function() {
	it("Should respond with json", function(done) {
		request(app)
		  .get('/messages' + generatePullQuery())
		  .set('Accept', 'application/json')
		  .expect('Content-Type', /json/)
		  .expect(validateResponse)
		  .expect(200, done)
		  
	});
});


describe('POST messages', function() {
	it("Should respond with json", function(done) {
		request(app)
		  .post('/messages')
		  .send(generateMessage())
		  .set('Accept', 'application/json')
		  .expect('Content-Type', /json/)
		  .expect(validateResponse)
		  .expect(200, done);
		  
	});
});


describe('GET /', function() {
	it("Should respond with html", function(done) {
		request(app)
		  .get('/' + generatePullQuery())
		  .expect('Content-Type', /html/)
		  .expect(200, done);
	});
});


function validateResponse(res) {
	console.log(res.body);
	var postsAreValid = true;
	if(!_.isArray(res.body.posts)) return "Posts array missing";
	
	if(!_.isObject(res.body.location)) return "location missing";
	
	if(!_.has(res.body.location, 'lat') || !_.has(res.body.location, 'lon')) return "Coordinates missing";
	
	
	_.each(res.body.posts, function(post) {
		if(!_.has(post, 'nick') || !_.has(post, 'msg') || !_.has(post, 'timestamp')) {
			postsAreValid = false;
		}
		
	});
	
	if(!postsAreValid) return "Invalid post";
}

function generateMessage() {
	return {nick: "Jose", msg: "blabla", 'location':{'lat':0.0, 'lon': 0.0}, 'timestampLastMsg': 0};
}

function generatePullQuery() {
	return '?location[lat]=0&location[lon]=0&timestampLastMsg=0';
}

