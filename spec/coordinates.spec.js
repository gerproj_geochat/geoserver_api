
var coords = require("../src/coordinates")
var _ = require("underscore");
/*
describe("Testing the sector method - deprecated", function() {
	it("Should parse the positive coordinates", function() {
		expect(coords.getSector({lat: 0.0001, lon: 0.2207})).toBe("0.0000_0.2205");
		expect(coords.getSector({lat: 0.0004, lon: 0.2209})).toBe("0.0000_0.2205");
		expect(coords.getSector({lat: 0.0005, lon: 0.2214})).toBe("0.0005_0.2210");
		expect(coords.getSector({lat: 0.9999, lon: 44.4444})).toBe("0.9995_44.4440");
	});

	it("Should parse the negatives coordinates", function() {
		expect(coords.getSector({lat: -0.0001, lon: 0.2207})).toBe("-0.0000_0.2205");
		expect(coords.getSector({lat: 0.0004, lon: -0.2209})).toBe("0.0000_-0.2205");
		expect(coords.getSector( {lat: -0.0005, lon: -0.2214} ) ).toBe("-0.0005_-0.2210");
		expect(coords.getSector({lat: -0.9999, lon: +44.4444})).toBe("-0.9995_44.4440");
	});

	it("Shouldn't accept out of range coordinates", function() {
		expect(coords.getSector({lat: 90.0001, lon: 0.2207})).toBeUndefined();
		expect(coords.getSector({lat: 91.0001, lon: 0.2207})).toBeUndefined();
		expect(coords.getSector({lat: -90.0001, lon: 0.2207})).toBeUndefined();
		expect(coords.getSector({lat: -91.0000, lon: 0.2207})).toBeUndefined();

		expect(coords.getSector({lat: 45.0001, lon: 90.2207})).toBeUndefined();
		expect(coords.getSector({lat: 60.0001, lon: 91.2207})).toBeUndefined();
		expect(coords.getSector({lat: 45.0001, lon: -90.2207})).toBeUndefined();
		expect(coords.getSector({lat: 60.0001, lon: -91.2207})).toBeUndefined();

		expect(coords.getSector({lat: -90.0001, lon: -90.2207})).toBeUndefined();
		expect(coords.getSector({lat: -90.0001, lon: -91.2207})).toBeUndefined();
		expect(coords.getSector({lat: -91.0001, lon: -90.2207})).toBeUndefined();
		expect(coords.getSector({lat: 11.2238, lon: 40.8822})).toBe(coords.getSector({lat: 11.2239, lon: 40.8824}));
	});

	it("Shouldn't accept anything that is not a coordinate", function() {
		expect(coords.getSector({})).toBeUndefined();
		expect(coords.getSector()).toBeUndefined();
		expect(coords.getSector(null)).toBeUndefined();
		expect(coords.getSector(undefined)).toBeUndefined();
		expect(coords.getSector(-1)).toBeUndefined();
		expect(coords.getSector("foo")).toBeUndefined();
		expect(coords.getSector(false)).toBeUndefined();
		expect(coords.getSector(true)).toBeUndefined();


	});

	it("Should parse coordinates with lower precision", function() {
		expect(coords.getSector({lat: 0.01, lon: 0.220})).toBe("0.0100_0.2200");
		expect(coords.getSector({lat: 0.1, lon: -0.9})).toBe("0.1000_-0.9000");
	});

	it("Should accept only hashes with keys lat and lon", function() {
		expect(coords.getSector({latitude: -0.0001, longitude: 0.2207})).toBeUndefined();
		expect(coords.getSector({latitude: 0.0001, longitude: 0.2207})).toBeUndefined();
	});
});
*/


describe("Range function", function() {

	var radius = 50; //meters (aproximadetly)


	it("Should return the right radius", function() {
		expect(coords.range({lat: 0.0, lon: 0.0}, radius)).toEqual(
			{min: {lat: -0.0005, lon: -0.0005}, max: {lat: 0.0005, lon: 0.0005} }
		);

		expect(_.map(coords.range({lat: 0.000007, lon: 0.0000003}, radius),function(next) {
			next.lat = next.lat.toFixed(4);
			next.lon = next.lon.toFixed(4);
			return next;
		})).toEqual(
			[ {lat: '-0.0005', lon: '-0.0005'}, {lat: '0.0005', lon: '0.0005'} ]
		);
	});

	it("Should only accept valid formated coordinates", function() {
		expect(coords.range({latitude: -0.0001, longitude: 0.2207}, radius)).toBeUndefined();

		expect(coords.range()).toBeUndefined();
	});

	it("Shoud not return invalid coordinates", function() {

	});
});



describe("Coordinates validity", function() {
	it("Should be valid", function() {
		expect(coords.isValid({lat: 0.0, lon: -0.1})).toBe(true);
		expect(coords.isValid({lat: -0.0, lon: 0.1})).toBe(true);
		expect(coords.isValid({lat: 86.0, lon: -180.0})).toBe(true);
		expect(coords.isValid({lat: -86.0, lon: -180.0})).toBe(true);
		expect(coords.isValid({lat: -86, lon: -180})).toBe(true);
		expect(coords.isValid({lat: 86.0, lon: 180})).toBe(true);
	});

	it("Should not be valid", function() {
		expect(coords.isValid({lat: 87.0, lon: 0.0})).toBe(false);
		expect(coords.isValid({lat: 86.0001, lon: 0.0})).toBe(false);
		expect(coords.isValid({lat: 0.7089, lon: 181.00})).toBe(false);
		expect(coords.isValid({lat: 86.0, lon: -180.0001})).toBe(false);
		expect(coords.isValid({lat: -87.0, lon: 1.0})).toBe(false);
	});
});
