/*globals $ document window geotools _ alert*/
if(document.getElementById('chat-page')) {
  (function() {
    //Page variables declarations

    //The area where the messages go
    var $chatArea = $('#chat-area');

    //The send message button
    var $sendButton = $('#send-button');

    //The textfield where the user types the messages
    var $msgInput = $('#msg-input');

    //The user nickname
    var nick = $('#nick').val();
    var facebookId = $('#facebookId').val();

    //The url to push the messages
    var URL_PUSH_MESSAGE = '/messages';

    //The url to pull the messages from
    var URL_PULL_MESSAGES = '/messages';

    //The timestamp of the last message returned by the server
    var timestampLastMsg = 0;

    //The last message receveid and printed
    var lastMsgPrinted = null;

    //The number of ajax errors received in the last seconds
    var ajaxErrorCount = 0;

    //The maximum number of ajax errors we can handle
    var MAXIMUM_AJAX_ERRORS = 3;

    //The number of miliseconds to wait until retry a request on error
    var RETRY_TIME = 500;
    var UPDATE_LOCATION_TIME = 4000;
    var PULL_TIME = 5*1000;

    var location = null;


    ////////////////////////////////////////////////////
    //Events declarations
    $sendButton.click(pushMessage);
    $msgInput.on('keypress', function(e) {
      var key = e.which;
      if(key == 13) { //the enter key
        pushMessage();
        return false;
      }
      return true;
    });

    //Resets ajax errors after 10 seconds
    window.setInterval(resetAjaxErrors, 10*1000);


    function pullMessages() {
      var reqData = {
        'location[lon]': location.coords.longitude,
        'location[lat]': location.coords.latitude,
        'timestampLastMsg': timestampLastMsg
      };

      //Do the ajax request
      $.get(URL_PULL_MESSAGES, reqData, handleServerResponse(function done(){
        window.setTimeout(pullMessages, PULL_TIME);
      }), 'json')
      .fail(handleAjaxError(pullMessages));
    }

    function updateLocation() {
      geotools.getLocation(function(recentLocation) {
        var oldLocation = location;
        location = recentLocation;
        if(!oldLocation) {
          pullMessages();
        }
        setTimeout(updateLocation, UPDATE_LOCATION_TIME);
      }, handleLocationError);
    }
    updateLocation();


    /////////////////////////////////////////////////////
    //Functions

    /**
    *	Post the current content of #msg-input to the server,
    * clears the input, and attach the server response to #chat-area.
    */
    function pushMessage() {
      var post = { nick: nick, msg: $msgInput.val(), facebookId: facebookId };
      if(isPostValid(post) && location) {
        $msgInput.val('');

        //Add the location to post data
        post['location[lat]'] = location.coords.latitude;
        post['location[lon]'] = location.coords.longitude;
        post['timestampLastMsg'] = timestampLastMsg;

        //Do a post request to the server
        $.post(URL_PUSH_MESSAGE, post, handleServerResponse(function() {

        }), 'json')
        .fail(handleAjaxError(pushMessage));
      }
    }

    /**
    * Handle the server response and put the messages on the screen
    * To be used inside a jQuery AJAX request. A callback can be passed
    */
    function handleServerResponse(done) {
      return function(data) {
        if(data) {
          var html = '';

          var autoscroll = isScrollOnBottom();
          var postsLength = data.posts.length;
          var next;
          var i;

          for(i = postsLength-1; i >= 0; i--) {
            next = data.posts[i];
            //Format the message
            if(!messageAlreadyReceived(next)) {
              html+=formatPost(next);
              lastMsgPrinted = next;

              //Updates the timestamp
              if(next.timestamp > timestampLastMsg) {
                timestampLastMsg = next.timestamp;
              }
            }
          }
          $chatArea.append(html);
          if(autoscroll) {
            scrollToBottom();
          }
          if(_.isFunction(done)) {
            done();
          }
        }
      };
    }


    /**
    * Verifies if a post has already been received.
    * @return true if affirmative, false otherwise
    */
    function messageAlreadyReceived(post) {
      if(post.timestamp < timestampLastMsg) {
        return true;
      }

      if(post.timestamp === timestampLastMsg
        && lastMsgPrinted && post.nick === lastMsgPrinted.nick
        && post.msg === lastMsgPrinted.msg
      ) {
        return true;
      }

      return false;
    }


    /**
    * Formats a post to be showed on the chat-area
    */
    function formatPost(post) {
      if(post) {
        return `
        <div class='media'>
          <div class='media-left'>
            <a href='#'>
              <img class='media-object img-circle' src='https://graph.facebook.com/v2.9/${post.facebook_id}/picture' alt='Foto de perfil'>
            </a>
            </div>
            <div class='media-body'>
              <div class='chat-message'>
                <p class='chat-message-content'>${post.msg}</p>
                <p class='small chat-message-name'>${post.nick}</h4>
              </div>

          </div>
        </div>`;
      }
    }

    function isScrollOnBottom() {
      return $(window).scrollTop() + $(window).height() == $(document).height();
    }
    function scrollToBottom() {
      $('html, body').animate({ scrollTop: $(document).height() }, 'slow');
    }


    function handleAjaxError(callback) {
      return function() {
        //If fails, then resend the post until it's done
        if(ajaxErrorCount < MAXIMUM_AJAX_ERRORS || !_.isFunction(callback)) {
          ajaxErrorCount++;

          setTimeout(function() {
            callback();
          }, RETRY_TIME);

        } else {
          redirectWithError('/', 'Connection to the server lost');
        }
      };
    }

    function redirectWithError(url, errorMessage) {
      alert(errorMessage);
      window.location.replace(url + '?errorMsg=' + errorMessage);
    }

    /**
    * Verifies if a post is valid by checking if it has message and nickname
    */
    function isPostValid(post) {
      if(post && post['nick'] && post['msg'] && _.isString(post['msg'])
      && post['msg'].length >= 1 && post['msg'].length <= 255) {
        return true;
      }
      return false;
    }

    /**
    * Handle any error in the GPS system.
    * It redirects to index with a error message.
    */
    function handleLocationError(error, message) {
      alert(message);
      window.location.replace('/?errorMsg=' + message);
    }

    /**
    * Reset the ajax error count
    */
    function resetAjaxErrors() {
      ajaxErrorCount = 0;
    }
  })();
}
