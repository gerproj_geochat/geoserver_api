/*globals document $*/
if(document.getElementById('thread-list')) {
  (function() {
    var PULL_TIMEOUT = 3000;

    function pullThreads() {
      $.get('/private_chat/threads', {}, function(threads) {
        var html = '';
        threads.forEach(function(thread) {
          html += formatThread(thread);
        });

        $('#thread-list').html(html);
        setTimeout(pullThreads, PULL_TIMEOUT);
      });
    }
    pullThreads();

    function formatThread(thread) {
      return `
        <div class="list-group-item" onclick="window.location.href = '/private_chat/thread/${thread.id}'">
          <div class="row-picture">
            <img class="circle" src="https://graph.facebook.com/v2.9/${thread.facebook_id}/picture" alt="icon">
          </div>
          <div class="row-content">
            <h4 class="list-group-item-heading">${thread.profile.displayName}</h4>

            <p class="list-group-item-text">${thread.unreadCount} mensagens não lidas</p>
          </div>
        </div>
      `;
    }

  })();
}
