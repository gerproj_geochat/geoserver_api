/*globals $ window */
(function() {

  var buildAPI = function(endpoint) {

    return {
      create: function(params, callback) {
        $.post(endpoint, params, callback);
      },
      retrieve: function(params, callback) {
        $.get(endpoint, params, callback);
      }
    };

  };

  window.AppearanceApi = buildAPI('/appearances');
  window.NearbyApi = buildAPI('/nearbies');

})();
