/*globals $ window document */
(function() {
  var didScroll;
  var lastScrollTop;
  var delta = 5;
  var $navbar = $('.navbar-hideable');
  var navbarHeight = $navbar.outerHeight();

  var $stickyTabs = $('.sticky-tabs');

  $(window).scroll(function() {
    didScroll = true;
  });

  setInterval(function() {
    if(didScroll) {
      handleScroll();
      didScroll = false;
    }
  }, 250);

  function handleScroll() {
    var scrollTop = $(window).scrollTop();
    if(Math.abs(lastScrollTop - scrollTop) <= delta) {
      return;
    }
    if(scrollTop > lastScrollTop && scrollTop > navbarHeight) {
      $navbar.removeClass('nav-down').addClass('nav-up');
      $stickyTabs.removeClass('tab-sticky-with-nav').addClass('tab-sticky-top');
    } else {
      // Scroll up if it did not scroll past the document (posible on max)
      if(scrollTop + $(window).height() < $(document).height()) {
        $navbar.removeClass('nav-up').addClass('nav-down');
        $stickyTabs.removeClass('tab-sticky-top').addClass('tab-sticky-with-nav');
      }
    }
    lastScrollTop = scrollTop;
  }

})();
