/*globals $ document geotools*/
$(document).ready(function() {
  var $loginButton = $('#login-button');
  var $errorLabel = $('#error-label');
  $.material.init();

  //if any error come from the server, show it
  if($errorLabel.html() && $errorLabel.html().length > 0) {
    $errorLabel.show();
  }

  $('#loginForm').submit(function(evt) {
    location.replace('/chat?nick=' + $('#nick').val());
    evt.preventDefault();
  });

  geotools.getLocation(function(location) {
    $loginButton.prop('disabled', false).html('<i class="fa fa-facebook-official"></i> Entrar com Facebook')
    .removeClass('btn-warning disabled')
    .addClass('btn-facebook');

  }, function(error, message) {
    $errorLabel.show('slow').html(message);
  });


});
