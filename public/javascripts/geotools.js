
(function() {
	
	//Safe reference to the window (global) object
	var root = this;
	
	//Constructor
	var geotools = function() {
		if(!(this instanceof geotools)) return new geotools();
	};
	
	
	//Exports the object to the global context
	root.geotools = geotools;
	
	
	geotools.getLocation = function(doneCallback, errorCallback) {
		//Try to get the user location
		if(navigator.geolocation) {
			
			
			navigator.geolocation.getCurrentPosition(doneCallback, showError(errorCallback));
		} else {
			
			errorCallback(null, "Error: Geolocation is not supported in your browser. It's not possible to use our service. "
				+ "Make sure the geolocation is active and your GPS is turned on.");
		}
	};
	
	
	function showError(errorCallback){
		return function(error) {
			var message;
			switch(error.code) 
			{
				case error.PERMISSION_DENIED:
					message.html("User denied the request for Geolocation.");
					break;
				case error.POSITION_UNAVAILABLE:
					message.html("Location information is unavailable.");
					break;
				case error.TIMEOUT:
					message.html("The request to get user location timed out.");
					break;
				case error.UNKNOWN_ERROR:
					message.html("An unknown error occurred.");
					break;
			}
			
			errorCallback(error, message);
		}
	}
	
}).call(this);
