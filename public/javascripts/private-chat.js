/*globals $ document window _ alert*/
if(document.getElementById('private-chat')) {
  (function() {
    //Page variables declarations

    //The area where the messages go
    var $chatArea = $('#private-chat-area');

    //The send message button
    var $sendButton = $('#private-send-button');

    //The textfield where the user types the messages
    var $msgInput = $('#private-msg-input');


    //The url to push the messages
    var URL_PUSH_MESSAGE = '/private_chat/messages';

    //The url to pull the messages from
    var URL_PULL_MESSAGES = '/private_chat/messages';

    var messages = {
      byId: {},
    };

    var chatUser = window.chatUser;
    var currentUser = window.currentUser;

    if(!chatUser) {
      alert('Erro. Nao foi possivel iniciar o chat. A pessoa que voce quer falar parece nao usar mais o aplicativo, ou voce pode ter sido bloqueado.');
      return;
    }


    //The number of ajax errors received in the last seconds
    var ajaxErrorCount = 0;

    var ENTER_KEY = 13;

    //The maximum number of ajax errors we can handle
    var MAXIMUM_AJAX_ERRORS = 3;

    //The number of miliseconds to wait until retry a request on error
    var RETRY_TIME = 500;
    var PULL_TIME = 2*1000;



    ////////////////////////////////////////////////////
    //Events declarations
    $sendButton.click(pushMessage);
    $msgInput.on('keypress', function(e) {
      var key = e.which;
      if(key == ENTER_KEY) {
        pushMessage();
        return false;
      }
      return true;
    });

    //Resets ajax errors after 10 seconds
    window.setInterval(resetAjaxErrors, 10*1000);


    function pullMessages() {
      var reqData = {
        fromId: currentUser.id,
        toId: chatUser.id,
      };

      //Do the ajax request
      $.get(URL_PULL_MESSAGES, reqData, handleServerResponse(function done(){
        window.setTimeout(pullMessages, PULL_TIME);
      }), 'json')
      .fail(handleAjaxError(pullMessages));
    }
    pullMessages();



    /////////////////////////////////////////////////////
    //Functions

    /**
    *	Post the current content of #msg-input to the server,
    * clears the input, and attach the server response to #chat-area.
    */
    function pushMessage() {
      var message = { body: $msgInput.val(), fromId: currentUser.id, toId: chatUser.id };
      if(isMessageValid(message)) {
        $msgInput.val('');

        //Do a message request to the server
        $.post(URL_PUSH_MESSAGE, message, handleServerResponse(function() {

        }), 'json')
        .fail(handleAjaxError(pushMessage));
      }
    }

    /**
    * Handle the server response and put the messages on the screen
    * To be used inside a jQuery AJAX request. A callback can be passed
    */
    function handleServerResponse(done) {
      return function(data) {
        if(data) {
          var html = '';

          var autoscroll = isScrollOnBottom();
          var messagesLength = data.messages.length;
          var next;
          var i;

          for(i = messagesLength-1; i >= 0; i--) {
            next = data.messages[i];
            //Format the message
            if(!messageAlreadyReceived(next)) {
              html+=formatMessage(next);
              messages.byId[next.id] = next;
            }
          }
          console.log(messages);
          $chatArea.append(html);
          if(autoscroll) {
            scrollToBottom();
          }
          if(_.isFunction(done)) {
            done();
          }
        }
      };
    }

    function messageAlreadyReceived(message) {
      return !!messages.byId[message.id];
    }

    function formatMessage(post) {
      if(post) {
        var user = post.from_id === chatUser.id ? chatUser : currentUser;
        
        return `
        <div class='media'>
          <div class='media-left'>
            <a href='#'>
              <img class='media-object img-circle' src='https://graph.facebook.com/v2.9/${user.facebook_id}/picture' alt='Foto de perfil'>
            </a>
            </div>
            <div class='media-body'>
              <div class='chat-message'>
                <p class='chat-message-content'>${post.body}</p>
                <p class='small chat-message-name'>${user.profile.displayName}</h4>
              </div>

          </div>
        </div>`;
      }
    }

    function isScrollOnBottom() {
      return $(window).scrollTop() + $(window).height() == $(document).height();
    }
    function scrollToBottom() {
      $('html, body').animate({ scrollTop: $(document).height() }, 'slow');
    }


    function handleAjaxError(callback) {
      return function() {
        //If fails, then resend the post until it's done
        if(ajaxErrorCount < MAXIMUM_AJAX_ERRORS || !_.isFunction(callback)) {
          ajaxErrorCount++;

          setTimeout(function() {
            callback();
          }, RETRY_TIME);

        } else {
          redirectWithError('/', 'Connection to the server lost');
        }
      };
    }

    function redirectWithError(url, errorMessage) {
      alert(errorMessage);
      window.location.replace(url + '?errorMsg=' + errorMessage);
    }

    /**
    * Verifies if a post is valid by checking if it has message and nickname
    */
    function isMessageValid(message) {
      return (message && _.isString(message.body)
        && message.body.length >= 1 && message.body.length <= 5000
      );
    }

    /**
    * Reset the ajax error count
    */
    function resetAjaxErrors() {
      ajaxErrorCount = 0;
    }
  })();
}
