/*globals $ currentUser*/
/*exported PrivateChatNotifications*/

if(currentUser) {
  var PrivateChatNotifications = (function() {
    var currentChatUser;
    var previousNotSeen = 0;
    var PULL_TIME = 5*1000;

    var pullNotifications = function() {
      $.get('/private_chat/not_seen', {currentChat: currentChatUser}, function(data) {
        if(data.notSeenCount && data.notSeenCount > 0 && data.notSeenCount !== previousNotSeen) {
          $.snackbar({content: `Voce tem ${data.notSeenCount} novas mensagens.`, timeout: 4000});
          previousNotSeen = data.notSeenCount;

          $('#unread-notifications').removeClass('hidden').html(data.notSeenCount);
        }
        setTimeout(pullNotifications, PULL_TIME);
      });
    };
    pullNotifications();


    return {
      setCurrentChatUser: function(chatUser) {
        currentChatUser = chatUser;
      }
    };
  })();

}
