/*globals AppearanceApi geotools currentUser document NearbyApi alert $*/
(function() {
  var PULL_TIME = 5000;
  var location;

  function updateLocation() {
    var UPDATE_LOCATION_TIME = 4000;
    geotools.getLocation(function(recentLocation) {
      var oldLocation = location;
      location = recentLocation;
      if(!oldLocation) {
        if(document.getElementById('nearbies')) {
          pullNearbies();
        }
      }
      setTimeout(updateLocation, UPDATE_LOCATION_TIME);
    }, function() {alert('Nao foi possivel carregar sua localizacao. Por favor habilite seu dispositivo GPS e autorize GeoChat a utilizar seu servico de navegacao');});
  }
  updateLocation();


  if(currentUser) {
    geotools.getLocation(function(location) {
      AppearanceApi.create({user_id: currentUser.id, lat: location.coords.latitude, lon: location.coords.longitude}, function(result) {
        console.log('success on creating appearance');
        console.log(result);
      });
    });
  }

  function pullNearbies() {
    var $nearbies = $('#nearbies');

    NearbyApi.retrieve({user_id: currentUser.id, lat: location.coords.latitude, lon: location.coords.longitude}, function(results) {
      if(results.users.length === 0) {
        console.log('sem aparicoes');
        $nearbies.html('Nao tem ninguem perto de voce no momento.');
      }
      var nearbiesHtml = results.users.map(function(user) {
        return `
          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="panel panel-default">
              <div class="panel-body">
                <h2 class="">${user.profile.displayName}</h2>
                <img class="img-responsive img-rounded" src="https://graph.facebook.com/v2.9/${user.facebook_id}/picture?height=500&width=500" alt="Foto de perfil">

                <a class="btn btn-primary" href="/private_chat/thread/${user.id}"><i class="fa fa-envelope-o"></i> Conversar</a>
              </div>
            </div>
          </div>
        `;
      }).join('');
      $nearbies.html(nearbiesHtml);
      setTimeout(pullNearbies, PULL_TIME);
    });
  }
})();
