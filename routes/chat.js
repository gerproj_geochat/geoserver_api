var _ = require("underscore");
var message = require("../src/message");

/*
* GET home page.
*/

exports.index = function(req, res){
  var errorMsg = '';
  if(req.query.errorMsg) {
    errorMsg = req.query.errorMsg;
  }
  res.render('index', {  'errorMsg': errorMsg, currentUser: {} });
};

exports.getChatWindow = function(req, res) {
  var nick = req.user.profile.displayName;
  var facebookId = req.user.facebook_id;
  res.render('chat', {'nick': nick, facebookId, currentUser: req.user });
};


exports.getMessages = function(req, res, next) {
  var get = req.query;
  if(!get.location || _.isUndefined(get.timestampLastMsg)) {
    //return res.send("Bad request - Location or timestampLastMsg missing", 400);
  }

  message.getInLocation(get.location, get.timestampLastMsg, function(err, data) {
    if(err) { return next(err); }

    //Reply back to the client
    res.json(data);

  });
};

exports.postMessage = function(req, res, next) {
  var post = {"nick": req.body.nick, "msg": req.body.msg, facebook_id: req.body.facebookId};

  message.postInLocation(req.body.location, post, function(err, data) {
    if(err) { return next(err); }

    if(data === false) {
      return res.send("bad request", 400);
    }

    //Reply back to the client
    message.getInLocation(req.body.location, req.body.timestampLastMsg, function(err, data) {
      if(err) { return next(err); }

      res.json(data);
      //next();
    });

  });
};
