const Storage = require('../src/storage');
const pool = require('../src/storage/pool');
const async = require('async');
exports.index = (req, res) => {
  res.render('private_chat', {currentUser: req.user});
};

exports.thread = (req, res) => {
  const userId = req.params.userId;
  if(!userId) {
    res.statusCode = 403;
    res.end('Forbidden');
  }

  Storage.findBy('users', 'id', userId).then((result) => {
    const user = result.rows[0];
    res.render('private_chat_thread', {currentUser: req.user, chatUser: user});
  }).catch(handleDbError(req, res));
};

exports.createMessage = (req, res) => {
  const message = {
    from_id: req.body.fromId,
    to_id: req.body.toId,
    body: req.body.body,
  };
  Storage.insert('messages', { attributes: message }).then(() => {
    return retrieveMessages(message.from_id, message.to_id);
  })
    .then(formatMessageList(req, res))
    .catch(handleDbError(req, res));
};

exports.listMessages = (req, res) => {
  readMessages(req.query.toId, req.query.fromId)
    .then(() => retrieveMessages(req.query.fromId, req.query.toId))
    .then(formatMessageList(req, res))
    .catch(handleDbError(req, res));
};

exports.notSeen = (req, res) => {
  const currentUserId = req.user.id;

  pool.query(`
    SELECT * FROM messages
    WHERE to_id = $1
    AND seen = $2`, [currentUserId, false]
  ).then((results) => {
    res.json({notSeenCount: results.rows.length});
  });
};

exports.listThreads = (req, res) => {
  const currentUserId = req.user.id;
  pool.query(`
    SELECT users.*
    FROM users
    WHERE id IN (
      SELECT from_id FROM messages WHERE to_id = $1
    ) OR id IN (
      SELECT to_id FROM messages WHERE from_id = $1
    )`, [currentUserId]
  ).then((results) => {
    async.map(results.rows,
      (user, callback) => {
        unread(user.id).then((unreadResults) => {
          const userWithUnread = Object.assign({}, user);
          userWithUnread.unreadCount = unreadResults.rows.length;
          callback(null, userWithUnread);
        });
      }, (err, results) => {
        res.json(results);
      });
  }).catch(handleDbError(req, res));
};

const unread = (userId) => {
  return pool.query(`
    SELECT *
    FROM messages
    WHERE from_id = $1
    AND seen = $2`, [userId, false]
  );
};

exports.destroyMessage = (req, res) => {

};


const retrieveMessages = (userAId, userBId) => {
  return pool.query(`
    SELECT * FROM messages
    WHERE (from_id = $1 OR from_id = $2)
    AND (to_id = $1 OR to_id = $2)
    ORDER BY created_at
  `, [userAId, userBId]);
};

const readMessages = (fromId, toId) => {
  return pool.query(`
    UPDATE messages
    SET seen = true
    WHERE from_id = $1
    AND to_id = $2
  `, [fromId, toId]);
};


const formatMessageList = (req, res) => (results) => {
  res.json({messages: results.rows });
};

const handleDbError = (req, res) => (error) => {
  console.error(error);
  res.statusCode(500);
  res.json({error: true, message: 'Internal server error. Message could not be sent'});
};
