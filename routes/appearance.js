const Storage = require('../src/storage');
const pool = require('../src/storage/pool');

exports.create = (req, res) => {
  Storage.insert('appearances', {attributes: req.body}).then((result) => {
    res.json(result);
  });
};


exports.list = (req, res) => {
  res.render('nearby', {currentUser: req.user});
};

exports.listNearbies = (req, res) => {
  //'SELECT * FROM appearances WHERE user_id != $1', [req.query.user_id]
  pool.query('SELECT * FROM USERS WHERE id IN (SELECT DISTINCT user_id FROM appearances WHERE user_id != $1)', [req.query.user_id], (err, results) => {
    res.json({
      users: results.rows
    });

  });
};
