# Geo Server - 0.1

Servidor para a aplicação GeoChat.

Este release contem a aplicação Web no mesmo servidor da API.

## Veja o demo
Acesse a aplicação em: https://unifei-geo-chat.herokuapp.com

## Executar aplicacao
1) Certifique-se que tenha as dependencias necessarias.
2) Clone o repositorio
3) Crie um banco de dados no postgresql chamado geo_chat
4) `yarn`
5) `yarn run setup`
6) `yarn start`

### Dependencias
Postgresql
NodeJs
Yarn
Git

## Documentação de projeto
O repositório com a documentação do projeto está disponivel em: https://gitlab.com/gerproj_geochat/geo_docs

A documentação foi separada do código fonte para facilitar a organização e otimizar o deploy para produção.

## Apresentado como projeto da disciplina de Gerencia de Projetos
Turma 2017.1

Universidade Federal de Itajuba

Prof. Adler


### Autores:
- Joao Paulo Motta Oliveira Silva, 24482
- Leandro de Lima Duarte, 24433
- Victor Rodrigues da Silva, 31054
