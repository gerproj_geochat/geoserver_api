### MIGRATION 1496063811413_createPosts (UP) ###
BEGIN;
CREATE TABLE "posts" (
  "id" serial PRIMARY KEY,
  "nick" varchar(20),
  "facebook_id" varchar(512),
  "msg" VARCHAR(255) NOT NULL,
  "creation_ts" TIMESTAMP DEFAULT NOW() NOT NULL,
  "lat" REAL NOT NULL,
  "lon" REAL NOT NULL
);
CREATE  INDEX  "posts_lat_lon_creation_ts_index" ON "posts" ("lat", "lon", "creation_ts");

COMMIT;


### MIGRATION 1496065548736_createUsers (UP) ###
BEGIN;
CREATE TABLE "users" (
  "id" serial PRIMARY KEY,
  "profile" jsonb NOT NULL,
  "facebook_id" varchar(512)
);
CREATE  UNIQUE  INDEX  "users_facebook_id_unique_index" ON "users" ("facebook_id");

COMMIT;


### MIGRATION 1496700964675_addAppearence (UP) ###
BEGIN;
CREATE TABLE "appearances" (
  "id" serial PRIMARY KEY,
  "user_id" integer NOT NULL REFERENCES users ON DELETE cascade ON UPDATE cascade,
  "lat" real NOT NULL,
  "lon" real NOT NULL,
  "created_at" timestamp DEFAULT NOW() NOT NULL
);
CREATE  INDEX  "appearances_user_id_index" ON "appearances" ("user_id");
CREATE  INDEX  "appearances_lat_lon_index" ON "appearances" ("lat", "lon");
CREATE  INDEX  "appearances_user_id_created_at_index" ON "appearances" ("user_id", "created_at");

COMMIT;

### MIGRATION 1497209303482_changeNickType (UP) ###
BEGIN;
ALTER TABLE "posts"
  ALTER "nick" SET DATA TYPE varchar(256);
INSERT INTO "public"."pgmigrations" (name, run_on) VALUES ('1497209303482_changeNickType', NOW());
COMMIT;


### MIGRATION 1497214823467_createMessage (UP) ###
BEGIN;
CREATE TABLE "messages" (
  "id" serial PRIMARY KEY,
  "to_id" integer NOT NULL REFERENCES users,
  "from_id" integer NOT NULL REFERENCES users,
  "created_at" timestamp DEFAULT NOW() NOT NULL,
  "body" varchar(5000) NOT NULL,
  "seen" boolean DEFAULT 'false' NOT NULL
);
INSERT INTO "public"."pgmigrations" (name, run_on) VALUES ('1497214823467_createMessage', NOW());
COMMIT;
