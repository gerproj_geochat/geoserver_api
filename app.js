/**
* Module dependencies.
*/

var express = require('express');
var chat = require('./routes/chat');
var privateChat = require('./routes/private_chat');
var appearance = require('./routes/appearance');
var http = require('http');
var path = require('path');

var passport = require('passport');
var facebookStrategy = require('./src/auth/facebookStrategy');

var app = express();


passport.use(facebookStrategy);

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('S3CRE7'));
app.use(express.cookieSession());
app.use(passport.initialize());
app.use(passport.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


//Test only, print errors
if('test' == app.get('env')) {
  console.log('Im on test');
  app.use(function(err, req, res, next) {
    if(err) {
      console.error('Error!', err);
      console.log(err.stack);
    }

    next(err);
  });
}

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


app.post('/appearances', appearance.create);
app.get('/appearances', appearance.list);
app.get('/nearbies', appearance.listNearbies);

app.get('/', chat.index);
app.get('/chat', chat.getChatWindow);

app.get('/private_chat', privateChat.index);
app.get('/private_chat/thread/:userId', privateChat.thread);
app.post('/private_chat/messages', privateChat.createMessage);
app.get('/private_chat/messages', privateChat.listMessages);
app.post('/private_chat/messages/:id/destroy', privateChat.destroyMessage);
app.get('/private_chat/not_seen', privateChat.notSeen);
app.get('/private_chat/threads', privateChat.listThreads);

app.get('/messages', chat.getMessages);
app.post('/messages', chat.postMessage);

app.get('/auth/facebook', passport.authenticate('facebook'));
app.get('/auth/facebook/callback',
  passport.authenticate('facebook', { successRedirect: '/chat',
    failureRedirect: '/'
  })
);
app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


exports.app = app;
