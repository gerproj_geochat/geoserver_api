const pool = require('./pool');

function mapKeys(attributes) {
  return Object.keys(attributes).map(key => [key, attributes[key]]);
}

module.exports.insert = function save(entity, { attributes, client = null }, callback) {
  const attrs = Object.assign({}, attributes);
  const connection = client || pool;

  if (attrs.updated_at === null) {
    delete attrs.updated_at;
  }

  const mappedKeys = mapKeys(attrs);
  const keys = mappedKeys.map(a => a[0]);


  const query = [`INSERT INTO ${entity}(${keys}) VALUES (`, mappedKeys.map((a, i) => `$${i + 1}`), ') RETURNING id'].join('');

  return connection.query(query, mappedKeys.map(a => a[1]), callback);
};

module.exports.destroyAll = function destroyAll(entity, callback) {
  return pool.query(['DELETE FROM', entity.name].join(' '), null, callback);
};

module.exports.findBy = (tableName, columnName, value, callback) => {
  let parsedValue = value;
  if(typeof value !== Array) {
    parsedValue = [value];
  }
  return pool.query(`SELECT * FROM ${tableName} WHERE ${columnName} = $1 LIMIT 1`, parsedValue, callback);
};
