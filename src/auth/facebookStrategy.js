const FacebookStrategy = require('passport-facebook').Strategy;
const config = require('config');
const UserIO = require('../user/io');

const FACEBOOK_APP_ID = config.get('facebook.appId');
const FACEBOOK_APP_SECRET = config.get('facebook.appSecret');

module.exports = new FacebookStrategy(
  {
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET,
    callbackURL: `${config.get('app.host')}/auth/facebook/callback`
  },
  function(accessToken, refreshToken, profile, done) {
    UserIO.findOrCreateByProfile(profile).then((results) => {
      done(null, results);
    }).catch(error => done(error));
  }
);
