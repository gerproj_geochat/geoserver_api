/**
 *
 *
 */
 var _ = require("underscore");
 
 
 exports.getSector = function(c) {
	//Checks if its a valid coordinate
	if(!exports.isValid(c)) {
		return undefined;
	}
	
	return parseSector(c['lat']) + "_" + parseSector(c['lon']);
 };
 
 
 /**
  * Gets the range of the coordinates inside that radius.
  * @params location Object {lat: numeric, lon: numeric}
  * @params radius numeric expressing the radius in meters
  * @returns an object {min: location, max: location} where location is an object with the same keys
  * as the one passed on parameter
	@returns undefined if the arguments are invalid
  */
 exports.range = range;
 function range(location, radius) {
	if(!isValid(location) || !_.isNumber(radius) || _.isNaN(radius)) {
		return undefined;
	}
	
	var factor = radiusFactor(radius);
	return 	{
				min: trim({lat: location.lat - factor, lon: location.lon - factor}),
				max: trim({lat: location.lat + factor, lon: location.lon + factor})
			};
 };
 
 function radiusFactor(radius) {
	return radius/100000;
 }
 
 function trim(coordinate) {
	if(coordinate.lat > 86.0) {
		coordinate.lat = 86.0;
	} else if(coordinate.lat < -86.0) {
		coordinate.lat = -86.0;
	}
	
	if(coordinate.lon > 180.0) {
		coordinate.lon = 180.0;
	} else if(coordinate.lon < -180.0) {
		coordinate.lon = -180.0;
	}
	
	return coordinate;
 }
 
 
 /**
  * Checks if a given coordinate is valid
  */
 exports.isValid = isValid; 
 function isValid(c) {
	if(_.isObject(c) && _.has(c, "lat") && _.has(c, "lon")) {
		//Parse the coords to float if they are strings
		if(_.isString(c.lat)) {
			c.lat = parseFloat(c.lat);
		}
		
		if(_.isString(c.lon)) {
			c.lon = parseFloat(c.lon);
		}
		
		
		if(!_.isNaN(c.lat) && !_.isNaN(c.lon) 
			&& c.lat >= -86.0  && c.lat <= 86.0 
			&& c.lon >= -180 && c.lon <= 180.0) {
				return true;
			}
	}
	
	return false;
 }
 
 function parseSector(coordinate) {
	var ret = coordinate.toFixed(4);
	var lastNum = ret.slice(-1);
	
	if(lastNum > 5) {
		return ret.slice(0, -1) + "5";
	} else if(lastNum === "5") {
		return ret;
	} else {
		return ret.slice(0, -1) + "0";
	}
 }