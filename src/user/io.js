const Storage = require('../storage');

const TABLE = 'users';

function findOrCreateByProfile(profile) {
  return Storage.findBy(TABLE, 'facebook_id', profile.id).then((results) => {
    if(!results.rows[0]) {
      return Storage.insert(TABLE, {
        attributes: {
          facebook_id: profile.id,
          profile,
        }
      }).then(results => results.rows[0]);
    } else {
      return results.rows[0];
    }
  });
}

exports.findOrCreateByProfile = findOrCreateByProfile;
