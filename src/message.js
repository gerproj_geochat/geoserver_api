//Requires
var _ = require("underscore");
var coords = require('./coordinates');
var pg = require('pg');
var pgConnectionString = require('./database').pgConnectionString;


//Constant variables
var OPERATION_RADIUS = 10*1000; //Meters
exports.OPERATION_RADIUS = OPERATION_RADIUS;

var PAGE_SIZE = 30;
exports.MAX_MSG_RETURNED = (function() { return PAGE_SIZE; })();


var TIME_TO_LIVE = 24*60*60; //Seconds (24 hours)

var NICK_MIN_LEN = 3;
var NICK_MAX_LEN = 20;

var MSG_MIN_LEN = 1;
var MSG_MAX_LEN = 255;

//Seek for next key
var NEXT_POST_KEY = 'globals:post';


exports.getInLocation = function(location, timestampLastMessage, done) {
	//Timestamp
	lastMessage = new Date();
	lastMessage.setTime(timestampLastMessage || 0);


	if(!coords.isValid(location)) {
		console.error("Invalid location: ");
		console.error(location);
		return done(new Error('Invalid location ' + location));
	}

	//Get a client from the pg pool
	pg.connect(pgConnectionString, function(err, client, pgDone) {
		if(err) return console.error('Error getting client from the pool', err);

		//Gets the range of messages to work with
		var cRange = coords.range(location, OPERATION_RADIUS);

		client.query('SELECT * FROM posts WHERE lat >= $1 AND lat <= $2 ' +
						'AND lon >= $3 AND lon <= $4 AND creation_ts > $5 ' +
						'ORDER BY id DESC ' +
						'LIMIT ' + PAGE_SIZE + ' OFFSET 0',
			[cRange.min.lat, cRange.max.lat, cRange.min.lon, cRange.max.lon, lastMessage]

		).on('row', function(row, result) {
			result.addRow(processIncomingPost(row));

		}).on('end', function(result) {
			pgDone();

			done(null, { "posts": result.rows, "location": location });


		}).on('error', function(error) {
			pgDone();
			console.error("Error searching for posts.", error);
			done(err);
		});

	});

};

/**
 * Returns a more user and bandwitch friendly post to be used.
 * The post is also valid
 */
function processIncomingPost(row) {
	row.timestamp = row.creation_ts.getTime();
	return _.omit(row, ['id', 'creation_ts', 'lat', 'lon']);
}


/**
 * Posts a message
 */
exports.postInLocation = function(location, post, done) {

	if(!coords.isValid(location) || !isPostValid(post)) {

		return done(null, false);
	}


	post.location = location;

	//Gets a pg connection from the pool
	pg.connect(pgConnectionString, function(err, client, donePg) {
		if(err) {
			console.error('Error fetching client from pool', err);
			return done(err);
		}

		//Post message
		client.query("INSERT INTO posts (nick, msg, lat, lon, facebook_id) VALUES ($1, $2, $3, $4, $5)",
			[post.nick, post.msg, post.location.lat, post.location.lon, post.facebook_id],
			function(err) {
				//Releases the client from the pool
				donePg();
				if(err) {
					console.error('Error inserting post.', err);
					done(err, false);
				}

				done(null, true);

			});

	});

}


exports.isPostValid = isPostValid;
function isPostValid(m) {
	if(_.isObject(m) && _.has(m, "nick") && _.has(m, "msg")) {
		if(isNickValid(m.nick) && isMessageValid(m.msg)) {
			return true;
		}
	}

	return false;
}


exports.isNickValid = isNickValid;
function isNickValid(nick) {
	if(_.isString(nick) && nick.length >= NICK_MIN_LEN) {
		return true;
	}

	return false;
}

exports.isMessageValid = isMessageValid;
function isMessageValid(msg) {
	if(_.isString(msg) && msg.length >= MSG_MIN_LEN  && msg.length <= MSG_MAX_LEN) {
		return true;
	}
	return false;
}
